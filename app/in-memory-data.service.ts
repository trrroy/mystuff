import { InMemoryDbService } from 'angular-in-memory-web-api';
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    console.log('createDB')
    let tasks = [
      {id: 11, name: 'task 11'},
      {id: 12, name: 'task 12'},
      {id: 13, name: 'task 13'},
      {id: 14, name: 'task 14'},
      {id: 15, name: 'task 15'},
      {id: 16, name: 'task 16'},
      {id: 17, name: 'task 17'},
      {id: 18, name: 'task 18'},
      {id: 19, name: 'task 19'},
      {id: 20, name: 'task 20'}
    ];
    let projects = [
      {id: 1, name: 'project 1'},
      {id: 2, name: 'project 2'},
      {id: 3, name: 'project 3'},
      {id: 4, name: 'project 4'},
      {id: 5, name: 'project 5'},
      {id: 6, name: 'project 6'},
      {id: 7, name: 'project 7'},
      {id: 8, name: 'project 8'},
      {id: 9, name: 'project 9'},
      {id: 0, name: 'project 10'}
    ];
    let expenses = [
      {id: 1, name: 'expense 1', cost: 21.24},
      {id: 2, name: 'expense 2', cost: 21.22},
      {id: 3, name: 'expense 3', cost: 21.27},
      {id: 4, name: 'expense 4', cost: 21.21},
      {id: 5, name: 'expense 5', cost: 21.29},
      {id: 6, name: 'expense 6', cost: 21.20},
      {id: 7, name: 'expense 7', cost: 21.23},
      {id: 8, name: 'expense 8', cost: 21.28},
      {id: 9, name: 'expense 9', cost: 21.27},
      {id: 0, name: 'expense 10', cost: 21.34}
    ];
    let invoices = [
      {id: 1, name: 'invoice 1'},
      {id: 2, name: 'invoice 2'},
      {id: 3, name: 'invoice 3'},
      {id: 4, name: 'invoice 4'},
      {id: 5, name: 'invoice 5'},
      {id: 6, name: 'invoice 6'},
      {id: 7, name: 'invoice 7'},
      {id: 8, name: 'invoice 8'},
      {id: 0, name: 'invoice 10'}
    ];
    let features = [
      {id: 1, name: 'feature 1'},
      {id: 2, name: 'feature 2'},
      {id: 3, name: 'feature 3'},
      {id: 4, name: 'feature 4'},
      {id: 5, name: 'feature 5'},
      {id: 6, name: 'feature 6'},
      {id: 7, name: 'feature 7'},
      {id: 8, name: 'feature 8'},
      {id: 0, name: 'feature 10'}
    ];
    let friends = [
      {id: 1, name: 'friend 1'},
      {id: 2, name: 'friend 2'},
      {id: 3, name: 'friend 3'},
      {id: 4, name: 'friend 4'},
      {id: 5, name: 'friend 5'},
      {id: 6, name: 'friend 6'},
      {id: 7, name: 'friend 7'},
      {id: 8, name: 'friend 8'},
      {id: 0, name: 'friend 10'}
    ];
    return {tasks, projects, expenses, invoices, features, friends};
  }
}


/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
