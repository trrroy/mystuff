import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Item } from './item';

@Injectable()
export class ItemService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private itemsUrl = 'api/';  // URL to web api

  constructor(private http: Http) { }

  getItems(t: string): Promise<Item[]> {
    console.log('getItems ' + t);
    return this.http.get(this.itemsUrl + t)
               .toPromise()
               .then(response => response.json().data as Item[])
               .catch(this.handleError);
  }


  getItem(t: string, id: number): Promise<Item> {
    const url = `${this.itemsUrl + t}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as Item)
      .catch(this.handleError);
  }

  delete(t: string, id: number): Promise<void> {
    const url = `${this.itemsUrl + t}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(t: string, name: string): Promise<Item> {
    return this.http
      .post(this.itemsUrl + t, JSON.stringify({name: name}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  update(t: string, item: Item): Promise<Item> {
    const url = `${this.itemsUrl + t}/${item.id}`;
    return this.http
      .put(url, JSON.stringify(item), {headers: this.headers})
      .toPromise()
      .then(() => item)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}



/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
