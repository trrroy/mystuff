import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ItemsComponent }      from './items.component';
import { ItemDetailComponent }  from './item-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/tasks', pathMatch: 'full' },
  { path: 'task/:id', component: ItemDetailComponent },
  { path: 'tasks',     component: ItemsComponent },
  { path: 'project/:id', component: ItemDetailComponent },
  { path: 'projects',     component: ItemsComponent },
  { path: 'expense/:id', component: ItemDetailComponent },
  { path: 'expenses',     component: ItemsComponent },
  { path: 'invoice/:id', component: ItemDetailComponent },
  { path: 'invoices',     component: ItemsComponent },
  { path: 'feature/:id', component: ItemDetailComponent },
  { path: 'features',     component: ItemsComponent },
  { path: 'friend/:id', component: ItemDetailComponent },
  { path: 'friends',     component: ItemsComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}


/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
