import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';

import { Item }                from './item';
import { ItemService }         from './item.service';

@Component({
  moduleId: module.id,
  selector: 'my-items',
  templateUrl: './items.component.html',
  styleUrls: [ './items.component.css' ]
})
export class ItemsComponent implements OnInit {
  items: Item[];
  sideitems: Item[];
  selectedItem: Item;

  constructor(
    private itemService: ItemService,
    private router: Router
  ) { }

  getItems(t: string): void {
    this.itemService
        .getItems(t)
        .then(items => this.items = items);
  }

  getSidebarItems(t: string): void {
    var n = t;
    switch (n) {
      case 'projects':
      case 'tasks':
        n = 'expenses';
        break;
    }
    if (t != n) {
      this.itemService
        .getItems(n)
        .then(sideitems => this.sideitems = sideitems);
    }
    else {
      //this.sideitems = Item[];
    }
  }

  getTotal(list){
    var total = 0;
    for(var i = 0; i < list.length; i++){
        total += list[i].cost;
    }
    return total;
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.itemService.create(name)
      .then(item => {
        this.items.push(item);
        this.selectedItem = null;
      });
  }

  delete(item: Item): void {
    this.itemService
        .delete(item.id)
        .then(() => {
          this.items = this.items.filter(h => h !== item);
          if (this.selectedItem === item) { this.selectedItem = null; }
        });
  }

  ngOnInit(): void {
    var url_parts = this.router.url.split('/');
    //url_parts.shift();
    console.log(url_parts);
    console.log("init --- " + url_parts[1]);
    this.getItems(url_parts[1]);
    this.getSidebarItems(url_parts[1]);
  }

  onSelect(item: Item): void {
    this.selectedItem = item;
  }

  gotoDetail(): void {
    var url_parts = this.router.url.split('/');
    this.router.navigate(['/' + url_parts[1].replace(/s$/, ''), this.selectedItem.id]);
  }
}


/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
