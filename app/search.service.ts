import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';

import { Observable }     from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Item }           from './item';

@Injectable()
export class ItemSearchService {

  constructor(private http: Http) {}

  search(term: string): Observable<Item[]> {
    return this.http
               .get(`app/items/?name=${term}`)
               .map(response => response.json().data as Item[]);
  }
}


/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
