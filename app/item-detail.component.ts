import 'rxjs/add/operator/switchMap';
import { Component, OnInit }      from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location }               from '@angular/common';

import { Item }        from './item';
import { ItemService } from './item.service';

@Component({
  moduleId: module.id,
  selector: 'my-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: [ './item-detail.component.css' ]
})
export class ItemDetailComponent implements OnInit {
  item: Item;
  sideitems: Item[];

  constructor(
    private itemService: ItemService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location
  ) {}

  ngOnInit(): void {
    var url_parts = this.router.url.split('/');
    this.route.params
      .switchMap((params: Params) => this.itemService.getItem(url_parts[1], +params['id']))
      .subscribe(item => this.item = item);
    this.getSidebarItems(url_parts[1]);
  }

  getSidebarItems(t: string): void {
    var n = t;
    switch (n) {
      case 'projects':
      case 'tasks':
        n = 'expenses';
        break;
    }
    if (t != n) {
      this.itemService
        .getItems(n)
        .then(sideitems => this.sideitems = sideitems);
    }
    else {
      //this.sideitems = Item[];
    }
  }

  getTotal(list){
    var total = 0;
    for(var i = 0; i < list.length; i++){
        total += list[i].cost;
    }
    return total;
  }
  save(): void {
    this.itemService.update(this.item)
      .then(() => this.goBack());
  }

  goBack(): void {
    this.location.back();
  }
}


/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
